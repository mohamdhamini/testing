@extends('layouts.app')
@section('content')
    <div class="row">
        {{--     User profile fild--}}
        <div class="col-xl-4 col-md-6 col-12">
            <div class="card profile-card-with-stats box-shadow-2">
                <div class="text-center">
                    <div class="card-body">
                        <img src="../../../app-assets/images/portrait/medium/avatar-m-10.png" class="rounded-circle  height-150" alt="Card image">
                    </div>
                    <div class="card-body vazir-font">
                        <h4 class="card-title vazir-font"> چاوش پناه</h4>
                        <ul class="list-inline list-inline-pipe vazir-font">
                            <li>مرد </li>
                            <li>پسماند یار</li>
                        </ul>
                        <h6 class="card-subtitle text-danger vazir-font"> موجودی کیف پول : 2200 تومان
                        </h6>
                    </div>
                    <p>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            نمایش مشخصات
                        </button>
                    </p>
                    <div class="collapse text-left" id="collapseExample">
                        <ul class="list-group">
                            <li class="list-group-item active">اقای چاوش امتیاز 2500</li>
                            <li class="list-group-item">رایان نامه : آدرس ایمیل کاربر </li>
                            <li class="list-group-item">شماره تماس :0221052</li>
                            <li class="list-group-item">کد کلی : 45642158798</li>
                            <li class="list-group-item">پیکان بار </li>
                            <li class="list-group-item"> سفید یخچالی</li>
                            <li class="list-group-item">ایران 57 د65958</li>
                        </ul>
                    </div>

                    <div class="card-body">
                        <p>
                           درخواست تصفیه حساب نداشته است
                        </p>
                        <button type="button" class="btn btn-outline-info btn-md btn-square mr-1"><i class="ft-user"></i> تصفیه حساب</button>
                        <button type="button" class="btn btn-outline-danger btn-md btn-square mr-1"><i class="la la-ban"></i>بلاک کردن کاربر </button>
                        <button type="button" class="btn btn-outline-primary btn-md btn-square mr-1"><i class="ft-user"></i> حذف کاربر</button>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
