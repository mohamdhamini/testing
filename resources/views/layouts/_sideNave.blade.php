{{--SideNav Ed HR--}}
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item vazir-font"><a href="{{route('index')}}"><i class="la la-home"></i><span
                        class="menu-title" data-i18n="nav.changelog.main">داشبورد</span></a></li>
            {{--           Breck List --}}
            {{--            <li class=" navigation-header">--}}
            {{--                <span data-i18n="nav.category.layouts">Layouts</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip"--}}
            {{--                                                                        data-placement="right" data-original-title="Layouts"></i>--}}
            {{--            </li>--}}

{{--            Walet Grops --}}
            <li class="nav-item vazir-font"><a href="{{route('wastes')}}"><i class="la la-archive"></i><span class="menu-title" data-i18n="nav.changelog.main">گروه بندی</span></a></li>
            {{--            Users List--}}
            <li class="nav-item vazir-font"><a href="#"><i class="la la-user"></i><span class="menu-title" data-i18n="nav.templates.main">کاربران</span></a>
                <ul class="menu-content vazir-font">
                    <li><a class="menu-item" href="{{route('users')}}" data-i18n="nav.templates.vert.main">تمامی کاربران</a></li>
                    <li><a class="menu-item" href="{{route('user')}}" data-i18n="nav.templates.vert.main">پسماند یار</a></li>
                    <li><a class="menu-item" href="{{route('driver')}}" data-i18n="nav.templates.vert.main">پسماند کار</a></li>
                    <li><a class="menu-item" href="{{route('admins')}}" data-i18n="nav.templates.vert.main">مدیران</a></li>
                </ul>
            </li>
            {{--           Servise  List--}}
            <li class="nav-item vazir-font"><a href="#"><i class="la la-television"></i><span class="menu-title"
                                                                                              data-i18n="nav.templates.main">سروریس ها</span></a>
                <ul class="menu-content vazir-font">
                    <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.main">سرویس های امروز</a></li>
                    <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.main">سرویس های هفته</a></li>
                    <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.main">سرویس های ماه</a></li>
                    <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.main">تمامی سرویس ها</a></li>
                </ul>
            </li>

            {{--            ACCOUNTING--}}
            <li class="nav-item vazir-font"><a href="#"><i class="la la-money"></i><span class="menu-title"
                                                                                        data-i18n="nav.templates.main">حسابداری</span></a>
                <ul class="menu-content vazir-font">
                    <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.main">اخرین پرداخت ها</a></li>
                    <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.main">پرداخت های این ماه</a></li>
                    <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.main">تصفیه حساب</a></li>
                    <li><a class="menu-item" href="#" data-i18n="nav.templates.vert.main">تمامی پرداخت ها</a></li>
                </ul>
            </li>

            <li class="nav-item vazir-font"><a href="#"><i class="la la-support"></i><span class="menu-title"
                                                                                           data-i18n="nav.support_raise_support.main">درباره ما</span></a>
            </li>
            <li class="nav-item vazir-font">
                <a href="#"><i class="la la-text-height"></i>
                    <span class="menu-title" data-i18n="nav.support_documentation.main">قوانین و مقررات</span>
                </a>
            </li>
        </ul>
    </div>
</div>
