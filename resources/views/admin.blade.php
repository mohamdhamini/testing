@extends('layouts.app')
@section('content')
    {{--   Information User And Report User--}}
    <div class="row">
        <div class="col-xl-3 col-lg-3 col-6">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-left">
                                <h3 class="info vazir-font">850
                                    نفر
                                </h3>
                                <h6 class="vazir-font">تعداد کل کاربران</h6>
                            </div>
                            <div>
                                <i class="icon-basket-loaded info font-large-2 float-right"></i>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-6">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-left">
                                <h3 class="warning vazir-font">748
                                    نفر
                                </h3>
                                <h6 class="vazir-font">پسماند یار</h6>
                            </div>
                            <div>
                                <i class="icon-pie-chart warning font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-6">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex ">
                            <div class="media-body text-left">
                                <h3 class="success vazir-font">146
                                    نفر
                                </h3>
                                <h6 class="vazir-font">پسماند کار</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow success font-large-2 float-right"></i>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-6">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-left">
                                <h3 class="danger vazir-font vazir-font">12</h3>
                                <h6 class="vazir-font">تعداد شکایات جدید</h6>
                            </div>
                            <div>
                                <i class="icon-heart danger font-large-2 float-right"></i>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--  Account Territory Users  --}}
    <div class="row vazir-font">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    درخواست تصفیه حساب
                </div>
{{--           Account Territory request     --}}
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-xl-3" >
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4 class="card-title info vazir-font">نام کاربر </h4>
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text text-danger"> مبلغ قابل پرداخت 200</p>
                                    <a href="#" class="btn btn-outline-info">پرداخت</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-3">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4 class="card-title info vazir-font">نام کاربر </h4>
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text text-danger"> مبلغ قابل پرداخت 200</p>
                                    <a href="#" class="btn btn-outline-info">پرداخت</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-3">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4 class="card-title info vazir-font">نام کاربر </h4>
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text text-danger"> مبلغ قابل پرداخت 200</p>
                                    <a href="#" class="btn btn-outline-info">پرداخت</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-3">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4 class="card-title info vazir-font">نام کاربر </h4>
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text text-danger"> مبلغ قابل پرداخت 200</p>
                                    <a href="#" class="btn btn-outline-info">پرداخت</a>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
{{--Fotter Account Territory--}}
                <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                     <span class="float-left">
                    <a href="#" class="card-link">نمایش همه  <i class="la la-angle-left"></i></a>
                  </span>

                    <span class="float-right"><span class="badge badge-pill badge-danger">5</span>
                         عدد باقی مانده
                    </span>

                </div>
            </div>
        </div>


    </div>
    {{--  Charity T Users  --}}
    <div class="row vazir-font">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                  کاربران برتر
                </div>
                {{--           Account Territory request     --}}
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-xl-3" >
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4 class="card-title info vazir-font">نام کاربر </h4>
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text text-danger"> امتیاز 200</p>
                                    <a href="#" class="btn btn-outline-info">نمایش</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-3">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4 class="card-title info vazir-font">نام کاربر </h4>
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text text-danger"> امتیاز 200</p>
                                    <a href="#" class="btn btn-outline-info">نمایش</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-3">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4 class="card-title info vazir-font">نام کاربر </h4>
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text text-danger"> امتتیاز 200</p>
                                    <a href="#" class="btn btn-outline-info">نمایش</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-3">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h4 class="card-title info vazir-font">نام کاربر </h4>
                                    <p class="card-text">

                                    </p>
                                    <p class="card-text text-danger"> مبلغ قابل پرداخت 200</p>
                                    <a href="#" class="btn btn-outline-info">پرداخت</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--Fotter Account Territory--}}
                <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                     <span class="float-left">
                    <a href="#" class="card-link">نمایش همه  <i class="la la-angle-left"></i></a>
                  </span>

                    <span class="float-right"><span class="badge badge-pill badge-danger">5</span>
                         عدد باقی مانده
                    </span>

                </div>
            </div>
        </div>


    </div>
@endsection
