@extends('layouts.app')
@section('content')
    {{--    Create Wastes--}}
    <div class="row">
        {{--     Group List Create   --}}
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-content">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="card-title vazir-font">گروه بندی </h4>
                                <h6 class="card-subtitle text-muted vazir-font">لیست گروه بندی های مواد بازیافتی </h6>
                            </div>
                            <div class="card-body">
                                {{--                                Form poste For Edit--}}
                                <form class="form" action="" method="post">
                                    @csrf
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label for="group_name" class="sr-only">نام گروه خود را وارد نمایید</label>
                                            <input type="text" id="group_name" class="form-control"
                                                   placeholder="نام گروه خود را وارد نمایید" name="wastes_group">
                                        </div>
                                        <div class="card">
                                            <div class="card-header">
                                                <label class="card-title" for="exampleInputFile">انتخاب ایکن یا تصویر
                                                    گروه بندی</label>
                                            </div>
                                            <div class="card-block">
                                                <div class="card-body">
                                                    <fieldset class="form-group">
                                                        <input type="file" class="form-control-file" id="group_icon">
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <button type="submit" class="btn btn-primary">ذخیره</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--        SubGroup List Create--}}
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <h4 class="card-title vazir-font">زیر گروه ها</h4>
                        <h6 class="card-subtitle text-muted vazir-font">متناسب با گروه بندی زیر گروه های خود را وارد
                            نمایید</h6>
                    </div>
                    <div class="card-body">
                        {{--                                Form poste For Edit--}}
                        <form class="form" action="" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Group_selected" class="sr-only">گروه بندی ها</label>
                                        <select id="Group_selected" name="Group_selected" class="form-control">
                                            <option value="none" selected="" disabled="">نوع مواد بازیافتی</option>
                                            <option value="design">کاغذ</option>
                                            <option value="development">فلز</option>
                                            <option value="illustration">پلاستیک</option>
                                            <option value="branding">شیشه</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="subgroup" class="sr-only">نام زیر گروه خود را انتخاب نمایید</label>
                                        <input type="text" id="subgroup" class="form-control"
                                               placeholder="نام زیر گروه خود را انتخاب نمایید" name="wastes_group">
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <label class="vazir-font">مشخصات عمومی و پایه بر اساس هر کیلو</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control"
                                                   placeholder="قیمت واحد بر مبنای تومان">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" placeholder="امتیاز واحد">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <button type="submit" class="btn btn-outline-primary">ثبت</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    Edit Wastes--}}
    <div class="row">
        {{--        CRUD subGroup Wastes--}}
        <div class="col-lg-6 col-md-12">
            {{--            Red View All ُSubGroup Wastes--}}
            <div class="card  text-center bg-transparent" style="height: 308.875px;">
                <div class="card-content">
                    <div class="card-body pt-3">
                        <img src="../../../app-assets/images/elements/11.png" alt="element 06" width="190"
                             class="float-right">
                        <h4 class="card-title mt-3 vazir-font">کاغذ</h4>
                        <p class="card-text text-info vazir-font">
                            و در صورت تمایل برای ایجاد اصلاحات دکمه ویرایش را فشار دهید
                        </p>
                        <p class="card-text text-danger vazir-font">توجه: در صورت حذف گروه بندی تمامی زیر گروه های مربوط به گروه مورد نظر نیز حذف میگردد</p>

                    {{--              Button trigger modal And Id Waste For Model delete          --}}
                    <!-- Button trigger modal  Delete-->
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_Group">
                            حذف
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="delete_Group" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title vazir-font" id="exampleModalLongTitle">حذف گروه کاغذ</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="text-danger text-left">
                                            توجه داشته باشید در صورت حذف گروه بندی تمامی زیر گروه های مرتبط با گروه اصلی نیز حذف خواهند شد در صورت تایید دکمه حذف و در غیر این صورت دکمه خروج را فشار دهید
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">خروج</button>
                                        <button type="button" class="btn btn-danger">حذف</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Button trigger modal And Id Waste For Model Edit-->
                        <button type="button" class="btn btn-blue" data-toggle="modal" data-target="#edit_wastes">
                            ویرایش
                        </button>

                        <!-- Modal Edit Wastes -->
                        <div class="modal fade bd-example-modal-lg" tabindex="-1" id="edit_wastes" role="dialog"
                             aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title vazir-font" id="exampleModalLongTitle">گروه بندی</h5>

                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card">
                                                    <div >
                                                        {{--                                Form poste For Edit--}}
                                                        <form class="form" action="" method="post">
                                                            @csrf
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label for="group_name" class="sr-only">تغییر نام گروه بندی</label>
                                                                    <input type="text" id="group_name" class="form-control" placeholder="نام گروه خود را وارد نمایید" name="wastes_group" value="کاغذ">
                                                                </div>
                                                                <div class="card">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label for="">انتخاب تصویر جدید</label>
                                                                                <input type="file" class="form-control-file" id="group_icon">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label for="">تصویر پیشین</label>
                                                                            <br>
                                                                            <img
                                                                                src="../../../app-assets/images/elements/11.png"
                                                                                alt="element 06" width="190"
                                                                                class="float-right">
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                            <div class="">
                                                                <button type="submit" class="btn btn-primary">ذخیره
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        {{--        CRUD Group Wastes--}}
        <div class="col-lg-6 col-md-12">
            {{--            Red View All Group Wastes--}}
            <div class="card" >
                <div class="card-header">
                    <h4 class="card-title vazir-font" id="list-editable">ویرایش یا حذف زیر گروه</h4>
                </div>
                <div class="card-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="Group_selected" class="sr-only">گروه بندی ها</label>
                                    <select id="Group_selected" name="Group_selected" class="form-control">
                                        <option value="none" selected="" disabled="">تمامی زیرگروه ها</option>
                                        <option value="design">کاغذ</option>
                                        <option value="development">فلز</option>
                                        <option value="illustration">پلاستیک</option>
                                        <option value="branding">شیشه</option>
                                    </select>
                                </div>
                            </div>
                        </div>
{{--                        SubGroup Wastes--}}
                        <div id="editable-list">
                         <div class="table-responsive">
                                <table class="table table-bordered table-lg text-center">
                                    <thead>
                                    <tr>
                                        <th class="sort text-center" data-sort="name">نام</th>
                                        <th class="sort text-center" data-sort="age">مبلغ واحد</th>
                                        <th class="sort text-center" data-sort="city">امتیاز واحد</th>
                                        <th>ویرایش</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <!-- IMPORTANT, class="list" have to be at tbody -->
                                    <tbody class="list">
                                    <tr>
                                        <td class="name">کاغذ</td>
                                        <td class="age">2500</td>
                                        <td class="city">200</td>

                                        <td class="edit">
{{--                                            Edit Sub Group Model--}}
                                            <!-- Button trigger modal And Id Waste For Model Edit-->
                                            <button type="button" class="btn btn-blue" data-toggle="modal" data-target="#edit_SubGroup_wastes">
                                                ویرایش
                                            </button>

                                            <!-- Modal Edit Wastes -->
                                            <div class="modal fade bd-example-modal-lg" tabindex="-1" id="edit_SubGroup_wastes" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title vazir-font" id="exampleModalLongTitle">ویرایش زیر گروه ها</h5>

                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="card">
                                                                <div class="card-content">
                                                                    <div class="card">
                                                                        <div >
                                                                            {{--                                Form poste For Edit--}}
                                                                            <form class="form" action="" method="post">
                                                                                @csrf
                                                                                <div class="form-body">
                                                                                    <div class="card-body">

                                                                                        <form class="form" action="" method="post">

                                                                                            <div class="row">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="form-group">
                                                                                                        <input type="text" id="subgroup" class="form-control" placeholder="نام زیر گروه خود را انتخاب نمایید" name="wastes_group">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-body">
                                                                                                <div class="row">
                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <input type="number" class="form-control" placeholder="قیمت واحد بر مبنای تومان">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <input type="number" class="form-control" placeholder="امتیاز واحد">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="">
                                                                                                <button type="submit" class="btn btn-outline-primary">ذخیره</button>
                                                                                            </div>
                                                                                        </form>

                                                                                    </div>
                                                                                </div>

                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
{{--Delet Sub Group MOdel--}}
                                        <td class="remove">
                                            <!-- Button trigger modal  Delete-->
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_SubGroup">
                                                حذف
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="delete_SubGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title vazir-font" id="exampleModalLongTitle">حذف زیر گروه روزنامه</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p class="text-danger text-left">
                                                                توجه داشته باشید در صورت حذف گروه بندی تمامی زیر گروه های مرتبط با گروه اصلی نیز حذف خواهند شد در صورت تایید دکمه حذف و در غیر این صورت دکمه خروج را فشار دهید
                                                            </p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">خروج</button>
                                                            <button type="button" class="btn btn-danger">حذف</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>

                            <p class="text-danger">نکات قابل توجه </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
