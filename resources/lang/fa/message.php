<?php
return [
    'isUserRegistered' => 'با این شماره قبلا در سیستم ثبت نام کرده اید',
    'sendsms_success' => 'پیامک با موفقیت ارسال شد',
    'send_not_sms' => 'مشکل در ارسال اس ام اس لطفا دوباره تلاش کنید!',
    'not_code_verify' => '!کد مطابقت ندارد',
    'verify_success' => 'تایید کد با موفقیت انجام شد',
    'expired_at' => 'کد فعال سازی منقضی شده است',
    'verificatian_code_already'=>'کد فعال سازی قبلا استفاده شده است',
    'user_success_active' => 'ثبت نام با موفقیت انجام شد',
    'authentication_failed' => 'موبایل یا نام کاربری اشتباه است.',
    'not_user_registered' => 'شما قبلا در سیستم ثبت نام نشده اید',
    'change_success_password'=>'پسورد با موفقیت تغییر یافت'
];
