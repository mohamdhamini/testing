<?php

namespace App;


class SMSVerificationCodeHelper
{
    public static function generateNewCode(): string
    {
        return substr(str_shuffle('0123456789'), 0, 5); 
    }
}
 