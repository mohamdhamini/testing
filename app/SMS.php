<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMS extends Model
{
    protected $table = 'sms_sent_logs';
    protected $casts = [
        'payload' => 'array'
    ];
}
