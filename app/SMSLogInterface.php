<?php
namespace App;

interface SMSLogInterface {
    public static function newVerificationSMSInstance(string $mobile, string $verificationCode): SMS;
}
