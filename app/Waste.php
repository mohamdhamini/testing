<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Waste extends Model
{
    public function wasteOrder(){
        return $this->hasMany(waste_order::class);
    }

    public function wasteType(){
        return $this->hasMany(wastse_type::class);
    }
}
