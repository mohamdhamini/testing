<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_queue_basket extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }
}
