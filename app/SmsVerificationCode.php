<?php

namespace App;
use DateInterval;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class SmsVerificationCode extends Model
{
    protected $table = 'sms_verification_codes';
    public static function hasActiveVerficationCode($mobile, string $scope = null)
    {
        $now = new \DateTime();
        return self::where('mobile', $mobile)
        ->where('expires_at', '>', $now)
        ->where('scope', $scope)
        ->first();
    }

    public static function saveNewCodeRequest(string $mobile, string $verificationCode, string $scope = null): SmsVerificationCode
    {
        $code = new SmsVerificationCode();
        $code->code = $verificationCode;
        $code->mobile = $mobile;
        $code->scope = $scope;

        $code->expires_at = (new SmsVerificationCode())->generateCodeExpirationFromNow();
        $code->save();

        return $code;
    }

    public function getVerificationCodeExpirationInterval(): DateInterval
    {
        return new DateInterval('PT5M');
    }

    public function generateCodeExpirationFromNow(): DateTime
    {
        $ex = new DateTime();
        $ex->add($this->getVerificationCodeExpirationInterval());
        return $ex;
    }

    public static function findVerificationCode(string $mobile, string $verificationCode, string $scope = null)
    {
        $qry = self::where('mobile', $mobile)
        ->where('code', $verificationCode);
        if ($scope) {
            $qry = $qry->where('scope', '=', $scope);
        }
        return $qry->first();
    }
}
