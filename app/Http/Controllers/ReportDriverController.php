<?php

namespace App\Http\Controllers;

use App\Report_driver;
use Illuminate\Http\Request;

class ReportDriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report_driver  $report_driver
     * @return \Illuminate\Http\Response
     */
    public function show(Report_driver $report_driver)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report_driver  $report_driver
     * @return \Illuminate\Http\Response
     */
    public function edit(Report_driver $report_driver)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report_driver  $report_driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report_driver $report_driver)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report_driver  $report_driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report_driver $report_driver)
    {
        //
    }
}
