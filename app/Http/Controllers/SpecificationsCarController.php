<?php

namespace App\Http\Controllers;

use App\Specifications_car;
use Illuminate\Http\Request;

class SpecificationsCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Specifications_car  $specifications_car
     * @return \Illuminate\Http\Response
     */
    public function show(Specifications_car $specifications_car)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Specifications_car  $specifications_car
     * @return \Illuminate\Http\Response
     */
    public function edit(Specifications_car $specifications_car)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Specifications_car  $specifications_car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Specifications_car $specifications_car)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Specifications_car  $specifications_car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Specifications_car $specifications_car)
    {
        //
    }
}
