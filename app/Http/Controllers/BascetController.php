<?php

namespace App\Http\Controllers;

use App\basket;
use Illuminate\Http\Request;

class BascetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\basket  $bascet
     * @return \Illuminate\Http\Response
     */
    public function show(basket $bascet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\basket  $bascet
     * @return \Illuminate\Http\Response
     */
    public function edit(basket $bascet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\basket  $bascet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, basket $bascet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\basket  $bascet
     * @return \Illuminate\Http\Response
     */
    public function destroy(basket $bascet)
    {
        //
    }
}
