<?php

namespace App\Http\Controllers;

use App\progress_order;
use Illuminate\Http\Request;

class ProgressOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\progress_order  $progress_order
     * @return \Illuminate\Http\Response
     */
    public function show(progress_order $progress_order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\progress_order  $progress_order
     * @return \Illuminate\Http\Response
     */
    public function edit(progress_order $progress_order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\progress_order  $progress_order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, progress_order $progress_order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\progress_order  $progress_order
     * @return \Illuminate\Http\Response
     */
    public function destroy(progress_order $progress_order)
    {
        //
    }
}
