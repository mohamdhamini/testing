<?php

namespace App\Http\Controllers;

use App\inistitutions;
use Illuminate\Http\Request;

class InistitutionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\inistitutions  $inistitutions
     * @return \Illuminate\Http\Response
     */
    public function show(inistitutions $inistitutions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\inistitutions  $inistitutions
     * @return \Illuminate\Http\Response
     */
    public function edit(inistitutions $inistitutions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\inistitutions  $inistitutions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, inistitutions $inistitutions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\inistitutions  $inistitutions
     * @return \Illuminate\Http\Response
     */
    public function destroy(inistitutions $inistitutions)
    {
        //
    }
}
