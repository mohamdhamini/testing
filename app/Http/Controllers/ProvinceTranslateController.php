<?php

namespace App\Http\Controllers;

use App\province_translate;
use Illuminate\Http\Request;

class ProvinceTranslateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\province_translate  $province_translate
     * @return \Illuminate\Http\Response
     */
    public function show(province_translate $province_translate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\province_translate  $province_translate
     * @return \Illuminate\Http\Response
     */
    public function edit(province_translate $province_translate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\province_translate  $province_translate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, province_translate $province_translate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\province_translate  $province_translate
     * @return \Illuminate\Http\Response
     */
    public function destroy(province_translate $province_translate)
    {
        //
    }
}
