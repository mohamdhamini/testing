<?php

namespace App\Http\Controllers;

use App\Basket_detail;
use Illuminate\Http\Request;

class BasketDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Basket_detail  $basket_detail
     * @return \Illuminate\Http\Response
     */
    public function show(Basket_detail $basket_detail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Basket_detail  $basket_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(Basket_detail $basket_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Basket_detail  $basket_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Basket_detail $basket_detail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Basket_detail  $basket_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Basket_detail $basket_detail)
    {
        //
    }
}
