<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
      return view('admin');
    }

    /*
     * functions For View Users Editor HR
     * */
    public function users()
    {
        return view('users_view.users');
    }

    public function user()
    {
        return view('users_view.user');
    }

    public function driver()
    {
        return view('users_view.driver');
    }

    public function admins()
    {
        return view('users_view.admins');
    }
    /*
     * end functions For View Users  Editor HR
     * */
}
