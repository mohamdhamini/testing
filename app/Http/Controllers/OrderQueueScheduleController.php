<?php

namespace App\Http\Controllers;

use App\order_queue_schedule;
use Illuminate\Http\Request;

class OrderQueueScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\order_queue_schedule  $order_queue_schedule
     * @return \Illuminate\Http\Response
     */
    public function show(order_queue_schedule $order_queue_schedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\order_queue_schedule  $order_queue_schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(order_queue_schedule $order_queue_schedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\order_queue_schedule  $order_queue_schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order_queue_schedule $order_queue_schedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\order_queue_schedule  $order_queue_schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(order_queue_schedule $order_queue_schedule)
    {
        //
    }
}
