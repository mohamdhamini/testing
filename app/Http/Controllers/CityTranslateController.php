<?php

namespace App\Http\Controllers;

use App\city_translate;
use Illuminate\Http\Request;

class CityTranslateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\city_translate  $city_translate
     * @return \Illuminate\Http\Response
     */
    public function show(city_translate $city_translate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\city_translate  $city_translate
     * @return \Illuminate\Http\Response
     */
    public function edit(city_translate $city_translate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\city_translate  $city_translate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, city_translate $city_translate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\city_translate  $city_translate
     * @return \Illuminate\Http\Response
     */
    public function destroy(city_translate $city_translate)
    {
        //
    }
}
