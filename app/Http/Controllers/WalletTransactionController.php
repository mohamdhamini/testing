<?php

namespace App\Http\Controllers;

use App\Wallet_transaction;
use Illuminate\Http\Request;

class WalletTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wallet_transaction  $wallet_transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Wallet_transaction $wallet_transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wallet_transaction  $wallet_transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Wallet_transaction $wallet_transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wallet_transaction  $wallet_transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wallet_transaction $wallet_transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wallet_transaction  $wallet_transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wallet_transaction $wallet_transaction)
    {
        //
    }
}
