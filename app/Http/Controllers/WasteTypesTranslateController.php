<?php

namespace App\Http\Controllers;

use App\waste_types_translate;
use Illuminate\Http\Request;

class WasteTypesTranslateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\waste_types_translate  $waste_types_translate
     * @return \Illuminate\Http\Response
     */
    public function show(waste_types_translate $waste_types_translate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\waste_types_translate  $waste_types_translate
     * @return \Illuminate\Http\Response
     */
    public function edit(waste_types_translate $waste_types_translate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\waste_types_translate  $waste_types_translate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, waste_types_translate $waste_types_translate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\waste_types_translate  $waste_types_translate
     * @return \Illuminate\Http\Response
     */
    public function destroy(waste_types_translate $waste_types_translate)
    {
        //
    }
}
