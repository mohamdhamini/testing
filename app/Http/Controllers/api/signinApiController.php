<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
//use Auth;

class signinApiController extends Controller
{
    public function login(Request $request){

       $validate = $request->validate([
            'mobile' => 'required',
            'password' => 'required',
        ]);

        $username = $request->input('mobile');
        $password = $request->input('password');

        if (! Auth::attempt(['mobile' => $username, 'password' => $password])) {
            // return abort(401, __('messages.authentication_failed'));
            return response()->json([
                'code' =>0,
                'message' => __('message.authentication_failed')
            ]);
        }

        $user = Auth::User();
        $data = $user->createToken('MyApp');
        return response()->json([
            'code' => 1,
            'data' => $data,
        ]);
    }
}
