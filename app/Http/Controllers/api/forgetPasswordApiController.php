<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\SmsVerificationCode;
use App\SMSVerificationCodeHelper;
use App\User;
use Illuminate\Http\Request;
use DateTime;
use DB;
use Hash;

class forgetPasswordApiController extends Controller
{
    public function sendSms(Request $request){
        $this->validate($request,[
            'mobile' => 'required',
        ]);
        $mobile = $request->input('mobile');
        $user = $this->validateIsUserRegistered($mobile);
        if (!$user){
            return response()->json([
                'code' => 0,
                'message' => __('message.not_user_registered')
            ]);
        }
        $verificationCode = SMSVerificationCodeHelper::generateNewCode();
//        $sender = "10004000500100";
//        $receptor = $mobile;
//        $message = "به اپلیکیشن پسماند خوش آمدید کد فعال سازی شما.$verificationCode.";
//        $api = new KavenegarApi("5152337A756B48656E63716C693038794146635A574D73636E2F366434682B617241374430444D6f5256673D");
//        $api->Send($sender, $receptor, $message);
//        if(! $api){
//            return response()->json([
//                'code' => 0,
//                'message' => __('message.send_not_sms'),
//            ]);
//        }


        SmsVerificationCode::saveNewCodeRequest($mobile, $verificationCode, 'forgetPassword');
        return response()->json([
            'code'=> '1',
            'message' => __('message.sendsms_success')
        ]);
    }


    private function validateIsUserRegistered(string $mobile)
    {
        $user = User::where('mobile', $mobile)->first();
        return $user;
    }


    public function changePassword(Request $request)
    {
        $this->validate($request,[
            'verification_code' => 'required',
            'mobile' => 'required',
            'password' => 'required|confirmed|min:8',
        ]);
        $mobile = $request->input('mobile');
        $password = Hash::make($request->input('password'));
        $verificationCode = $request->input('verification_code');
        $code = SMSVerificationCode::findVerificationCode($mobile, $verificationCode, 'forgetPassword');
        if (!$code){
            return response()->json([
                'code' => 0,
                'message' => __('message.not_code_verify')
            ]);
        }
        $now = new DateTime();
        $exp = new DateTime($code->expires_at);
        if ($now > $exp){
            return response()->json([
                'code' => 0,
                'message' => __('message.expired_at')
            ]);
        }
        if ($code->verified){
            return response()->json([
                'code' => 0,
                'message' => __('message.verificatian_code_already')
            ]);
        }
        $code->verified = true;
        $code->save();

        $set = DB::table('users')->where('mobile' , $mobile)->update(['password' => $password]);

        if($set){
            return response()->json([
                'code' => 1,
                'message' => __('message.change_success_password')
            ]);
        }


    }
}
