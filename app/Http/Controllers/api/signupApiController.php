<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\SmsVerificationCode;
use App\SMSVerificationCodeHelper;
use App\User;
use DateTime;
use http\Env\Response;
use Illuminate\Http\Request;
use Kavenegar\KavenegarApi;
use vendor\Kavenegar;
use Hash;
use DB;


class signupApiController extends Controller
{
    //public function send sms for sinUp
    public function sendSms(Request $request)
    {
        $validate = $this->validate($request, [
            'mobile' => 'required',
        ]);
        $mobile = $request->input('mobile');
        $user = $this->validateIsUserRegistered($mobile);
        if ($user) {
            return response()->json([
                'code' => 0,
                'message' => __('message.isUserRegistered'),
            ]);
        }
        $verificationCode = SMSVerificationCodeHelper::generateNewCode();
//        $sender = "10004000500100";
//        $receptor = $mobile;
//        $message = "به اپلیکیشن پسماند خوش آمدید کد فعال سازی شما.$verificationCode.";
//        $api = new KavenegarApi("5152337A756B48656E63716C693038794146635A574D73636E2F366434682B617241374430444D6f5256673D");
//        $api->Send($sender, $receptor, $message);
//        if(! $api){
//            return response()->json([
//                'code' => 0,
//                'message' => __('message.send_not_sms'),
//            ]);
//        }
        SmsVerificationCode::saveNewCodeRequest($mobile, $verificationCode, 'register');
        return response()->json([
            'code' => 1,
            'message' => __('message.sendsms_success'),
        ]);
    }

    // exist mobile in table user
    private function validateIsUserRegistered(string $mobile)
    {
        $user = User::where('mobile', $mobile)->first();
        return $user;
    }

    //verify code send to user
    public function verifyCode(Request $request)
    {
        $this->validate($request, [
            'mobile' => 'required',
            'verification_code' => 'required'
        ]);
        $mobile = $request->input('mobile');
        $verificationCode = $request->input('verification_code');
        $code = SMSVerificationCode::findVerificationCode($mobile, $verificationCode, 'register');
        if (! $code){
            return response()->json([
                'code' => 0,
                'message' => __('message.not_code_verify'),
            ]);
        }

        $now = new DateTime();
        $exp = new DateTime($code->expires_at);
        if ($now > $exp){
            return response()->json([
                'code' => 1,
                'message' => __('message.expired_at')
            ]);
        }
        if ($code->verified){
            return response()->json([
                'code' => 1,
                'message' => __('message.verificatian_code_already')
            ]);
        }
        $code->verified = true;
        $code->save();
        return response()->json([
            'code' => 1,
            'message' => __('message.verify_success'),
        ]);

    }

    public function setPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|string|min:8',
            'mobile' => 'required',
            'role' => 'required'
        ]);

        $password = Hash::make($request->input('password'));
        $mobile = $request->input('mobile');
        $role = $request->input('role');
        $hasUser = $this->validateIsUserRegistered($mobile);
        if ($hasUser){
            return response()->json([
                'code' => 1,
                'message' => __('message.isUserRegistered')
            ]);
        }
        $user = new User();
        $user->mobile = $mobile;
        $user->password = $password;
        $user->role = $role;
        $user->status = true;
        $user->save();
        // DB::commit();

         return response()->json([
                'code' => 1,
                'message' => __('message.user_success_active')
            ]);
    }
}
