<?php

namespace App\Http\Controllers;

use App\wastes_translate;
use Illuminate\Http\Request;

class WastesTranslateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\wastes_translate  $wastes_translate
     * @return \Illuminate\Http\Response
     */
    public function show(wastes_translate $wastes_translate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\wastes_translate  $wastes_translate
     * @return \Illuminate\Http\Response
     */
    public function edit(wastes_translate $wastes_translate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\wastes_translate  $wastes_translate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, wastes_translate $wastes_translate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\wastes_translate  $wastes_translate
     * @return \Illuminate\Http\Response
     */
    public function destroy(wastes_translate $wastes_translate)
    {
        //
    }
}
