<?php

namespace App\Http\Controllers;

use App\wastse_type;
use Illuminate\Http\Request;

class WastseTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\wastse_type  $wastse_type
     * @return \Illuminate\Http\Response
     */
    public function show(wastse_type $wastse_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\wastse_type  $wastse_type
     * @return \Illuminate\Http\Response
     */
    public function edit(wastse_type $wastse_type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\wastse_type  $wastse_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, wastse_type $wastse_type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\wastse_type  $wastse_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(wastse_type $wastse_type)
    {
        //
    }
}
