<?php

namespace App\Http\Controllers;

use App\Driver_report;
use Illuminate\Http\Request;

class DriverReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Driver_report  $driver_report
     * @return \Illuminate\Http\Response
     */
    public function show(Driver_report $driver_report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Driver_report  $driver_report
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver_report $driver_report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Driver_report  $driver_report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Driver_report $driver_report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Driver_report  $driver_report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver_report $driver_report)
    {
        //
    }
}
