<?php

namespace App\Http\Controllers;

use App\waste_order;
use Illuminate\Http\Request;

class WasteOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\waste_order  $waste_order
     * @return \Illuminate\Http\Response
     */
    public function show(waste_order $waste_order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\waste_order  $waste_order
     * @return \Illuminate\Http\Response
     */
    public function edit(waste_order $waste_order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\waste_order  $waste_order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, waste_order $waste_order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\waste_order  $waste_order
     * @return \Illuminate\Http\Response
     */
    public function destroy(waste_order $waste_order)
    {
        //
    }
}
