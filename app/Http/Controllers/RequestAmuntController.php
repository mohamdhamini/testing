<?php

namespace App\Http\Controllers;

use App\Request_amunt;
use Illuminate\Http\Request;

class RequestAmuntController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Request_amunt  $request_amunt
     * @return \Illuminate\Http\Response
     */
    public function show(Request_amunt $request_amunt)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Request_amunt  $request_amunt
     * @return \Illuminate\Http\Response
     */
    public function edit(Request_amunt $request_amunt)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Request_amunt  $request_amunt
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Request_amunt $request_amunt)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Request_amunt  $request_amunt
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request_amunt $request_amunt)
    {
        //
    }
}
