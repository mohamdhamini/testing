<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class waste_order extends Model
{
    public function waste(){
        return $this->belongsTo(Waste::class);
    }
}
