<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baskets', function (Blueprint $table) {
            $table->id();
            $table->integer('status')->default(0);
            $table->bigInteger('user_id')->unsigned();
//            $table->bigInteger('transaction_id')->unsigned();
            $table->bigInteger('driver_id')->unsigned()->nullable();
            $table->date('deliver_time')->nullable();
//            this field fill when driver set real weight
            $table->bigInteger('total_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baskets');
    }
}
