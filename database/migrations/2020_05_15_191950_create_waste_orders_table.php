<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWasteOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waste_orders', function (Blueprint $table) {
            $table->id();
//            $table->bigInteger('order_queue_id');
            $table->bigInteger('waste_id')->unsigned();
            $table->bigInteger('single_price');
            $table->Integer('total_row_price');
            $table->timestamps();
//
//            $table->foreign('order_queue_id')
//                ->on('order_queue_baskets')
//                ->references('id')
//                ->onDelete('cascade')
//                ->onUpdate('cascade');

            $table->foreign('waste_id')
                ->on('wastes')
                ->references('id')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waste_orders');
    }
}
