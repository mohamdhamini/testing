<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charities', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('inistitution_id')->unsigned();
            $table->integer('amount');
            $table->timestamps();

//            $table->foreign('user_id')
//            ->on('users')
//            ->references('id')
//            ->onDelete('cascade');
//
//            $table->foreign('inistitution_id')
//            ->on('inistitutions')
//            ->references('id')
//            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charities');
    }
}
