<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
//            ایدی پرداخت کننده
            $table->bigInteger('sender_user_id')->unsigned();
//            ایدی دریافت کننده
            $table->bigInteger('reviver_userId')->unsigned();
//            مبلغ انتقال
            $table->bigInteger('amount');
//             کد تراکنش
            $table->bigInteger('refID');
            $table->date('date');
//          عدد 0 = شارژ عدد 1 = خرید پسماند عدد 2 = کمک به نیازمندان
            $table->string('type');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
