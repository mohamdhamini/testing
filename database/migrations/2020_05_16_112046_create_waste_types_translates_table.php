<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWasteTypesTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waste_types_translates', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('language_id')->unsigned();
            $table->bigInteger('waste_type_id')->unsigned();
            $table->string('waste_type');
            $table->timestamps();

//            $table->foreign('language_id')
//                ->on('languages')
//                ->references('id');
//
//            $table->foreign('waste_type_id')
//                ->on('waste_type')
//                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waste_types_translates');
    }
}
