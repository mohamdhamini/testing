<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_drivers', function (Blueprint $table) {
//            گزارش وضعیت بازیلافت محصولات توسطه راننده
            $table->id();
//            ارتباط به ایدی راننده
            $table->bigInteger('user_id')->unsigned();
//            نمایش محصولی که راننده دریافت کرده
            $table->bigInteger('bascet_id')->unsigned();
//            ایا کاربر محصول را از راننده گرفته یا لغو کرده
            $table->boolean('reject_user')->default(0);
//            آیا راننده انجام وظیفه را لغو نموده
            $table->boolean('reject_driver')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_drivers');
    }
}
