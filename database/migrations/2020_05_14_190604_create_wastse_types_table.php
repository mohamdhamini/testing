<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWastseTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wastse_types', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('waste_id')->unsigned();
//            قیمت واحد
            $table->Integer('price');
//            امتیاز واحد هر گروه
            $table->bigInteger('score');
            $table->string('name');
            $table->timestamps();

            $table->foreign('waste_id')
                ->on('wastes')
                ->references('id')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wastse_types');
    }
}
