<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('national_code')->nullable();
            $table->string('mobile')->unique();
            $table->char('role',2)->nullable();
            $table->boolean('admin')->default(0);
//            مقدار پ.ول
            $table->string('balance')->nullable();
//            ارتباط به جدول امتیازات
            $table->string('score_id')->nullable();
//            وضعیت کاربر
            $table->boolean('status')->default(0);
//            جنسیت
            $table->enum('gender', ['f', 'm'])->nullable();
//            شماره شباب بانکی
            $table->string('sheba_number')->nullable();
//            تصویر کاربر
            $table->string('avatar')->nullable();
            $table->text('fcm_token')->nullable();
//            بلوک کردن کاربر
            $table->boolean('blocked')->default(0);
//            مشخصات خودرو کاربر
            $table->string('car')->nullable();
            $table->string('color')->nullable();
            $table->string('pelak')->nullable();
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
