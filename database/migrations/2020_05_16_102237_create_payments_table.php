<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->decimal('amount', 24, 4);
            $table->string('gateway_name');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->boolean('was_success')->default(false)->comment('پیش فرض ناموفق است مگر اینکه در زمان تایید موفقیت نهایی تغییر کند');

            $table->integer('payment_result_code')->nullable();
            $table->string('payment_result_string')->nullable();

            $table->integer('verify_result_code')->nullable();
            $table->string('verify_result_string')->nullable();

            $table->string('track_id', 512)
                ->unique()
                ->collation('latin1_general_ci')
                ->nullable();

            $table->text('other_payloads')->nullable()->comment('در صورتیکه در نتایج پرداخت داده ای جز موارد موجود وجود داشته باشد در این بخش ثبت میشود');

            $table->timestamps();


            $table->foreign('user_id')
                ->on('users')
                ->references('id')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
