<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('province_id')->unsigned();
            $table->string('name');
            $table->float('gp_lat',10,8);
            $table->float('gp_lan',10,8);
//            service radius
            $table->integer('distance');
//            Default 0 =City Is not Active And 1= Is active
            $table->boolean('active')->default(0);
            $table->timestamps();
            $table->foreign('province_id')
                  ->on('provinces')
                  ->references('id')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
