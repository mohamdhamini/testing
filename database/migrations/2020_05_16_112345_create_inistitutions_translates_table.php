<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInistitutionsTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inistitutions_translates', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('language_id')->unsigned();
            $table->bigInteger('inistitutions_id')->unsigned();
            $table->string('inistitutions');
            $table->timestamps();
            $table->foreign('language_id')
                ->on('languages')
                ->references('id');

            $table->foreign('inistitutions_id')
                ->on('inistitutions')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inistitutions_translates');
    }
}
