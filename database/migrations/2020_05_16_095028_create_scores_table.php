<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
//            امتیاز واحد
            $table->bigInteger('point');


//            $table->bigInteger('user_customer')->unsigned();
//            $table->bigInteger('user_driver')->unsigned();
//            $table->float('score');
//            $table->bigInteger('order_waste_id')->unsigned();
//            $table->timestamps();
//
//
//
//            $table->foreign('user_driver')
//                ->on('users')
//                ->references('id')
//                ->onDelete('cascade');
//
//            $table->foreign('order_waste_id')
//                ->on('waste_orders')
//                ->references('id')
//                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scores');
    }
}
