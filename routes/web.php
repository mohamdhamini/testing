<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','AdminController@index')->name('index');
Route::get('/wastes','WasteController@index')->name('wastes');
Route::get('/users','AdminController@users')->name('users');
Route::get('/user','AdminController@user')->name('user');
Route::get('/driver','AdminController@driver')->name('driver');
Route::get('/admins','AdminController@admins')->name('admins');
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
