<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//route group register
Route::group(['prefix' => 'signUp'],function(){
    Route::post('/sendSms' , 'api\signupApiController@sendSms');
    Route::post('/verify' , 'api\signupApiController@verifyCode');
    Route::post('/setpassword' , 'api\signupApiController@setPassword');
});

// //route login
Route::post('/login' , 'api\signinApiController@login');

// //route group forget password
Route::group(['prefix' => 'forgetPassword'],function(){
    Route::post('/sendSms' , 'api\forgetPasswordApiController@sendSms');
    Route::post('/changePassword' , 'api\forgetPasswordApiController@changePassword');
}) ;
